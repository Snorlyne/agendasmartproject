window.addEventListener('load', function () {
  setTimeout(() => {
    const element = document.getElementById('loader')
    if (element) {
      element.style.display = 'none'
    }
  }, 2000)
})
