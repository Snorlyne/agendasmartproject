import AuthService from './AuthServices'
import type IGroup from '@/interfaces/IGroup'
import type { IResponse } from '@/interfaces/IResponse'

const auth = new AuthService()
const url = 'https://agendasmartproject.onrender.com/api/v1'

export default class TaskService {
  private response: IResponse = {} as IResponse
  constructor() {}

  async getGroups(): Promise<any> {
    try {
      const response = await fetch(url + '/groups', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return jsonResponse
    } catch (error) {
      console.error('Error:', error)
    }
  }
  async getGroup(id: number): Promise<any> {
    try {
      const response = await fetch(url + '/groups/' + id, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return jsonResponse
    } catch (error) {
      console.error('Error:', error)
    }
  }
  async createGroup(group: IGroup): Promise<any> {
    try {
      const response = await fetch(url + '/groups', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify(group)
      })
      const json = await response.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.error(error)
    }
  }
  async deleteGroup(id: number): Promise<any> {
    try {
      const response = await fetch(url + '/groups/' + id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const json = await response.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.error(error)
    }
  }
}
