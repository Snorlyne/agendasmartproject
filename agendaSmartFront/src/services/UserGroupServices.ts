import type { IResponse } from '@/interfaces/IResponse'
import AuthService from './AuthServices'

const auth = new AuthService()
const url = 'https://agendasmartproject.onrender.com/api/v1'

export default class TaskService {
  private response: IResponse = {} as IResponse
  constructor() {}

  async joinGroup(keyGroup: string): Promise<IResponse> {
    try {
      const response = await fetch(`${url}/users-groups/join`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify({
          keyGroup: keyGroup
        })
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }
  async leaveGroup(idGroup: number): Promise<IResponse> {
    try {
      const response = await fetch(`${url}/users-groups/leave`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify({
          idGroup: idGroup
        })
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }
  async getUsersGroup(id: number): Promise<IResponse> {
    try {
      const response = await fetch(`${url}/users-groups/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }
  async expelUserGroup(id: number): Promise<IResponse> {
    try {
      const response = await fetch(`${url}/users-groups/expel/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }
}
