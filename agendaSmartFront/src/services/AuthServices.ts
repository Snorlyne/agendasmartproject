import router from '@/router'
import Swal from 'sweetalert2'
import { ref, type Ref } from 'vue'
import type { IResponse } from '@/interfaces/IResponse'

const url = 'https://agendasmartproject.onrender.com/api/v1'

export default class AuthService {
  private token: Ref<string>
  private response: IResponse = {} as IResponse
  constructor() {
    this.token = ref('')
  }

  async login(emailOrUsername: string, password: string): Promise<IResponse> {
    const formData = { emailOrUsername, password }

    try {
      const response = await fetch(url + '/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })

      const jsonResponse = await response.json()
      if (response.ok) {
        this.token = jsonResponse.data
        return {
          data: this.token,
          message: jsonResponse.message,
          error: jsonResponse.error
        }
      } else {
        return {
          data: jsonResponse.data,
          message: jsonResponse.message,
          error: jsonResponse.error
        }
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }

  async register(username: string, email: string, password: string): Promise<IResponse> {
    try {
      const formData = { username, email, password }
      const response = await fetch(url + '/auth/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
      const jsonResponse = await response.json()
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      this.response = {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
    return this.response
  }

  verifyToken(data: any): any {
    const { message, error } = data
    if (error && message === 'La sesión ha expirado') {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: message,
        allowEscapeKey: false,
        confirmButtonText: `
                    Ir al inicio de sesión
                `
      }).then((tops) => {
        if (tops.isConfirmed) {
          localStorage.clear()
          router.push({ name: 'login' })
        }
      })
    }
  }
}
