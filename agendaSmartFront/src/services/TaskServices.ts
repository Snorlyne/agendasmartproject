import AuthService from './AuthServices'
import type ITask from '@/interfaces/ITasks'
import type { IResponse } from '@/interfaces/IResponse'

const auth = new AuthService()
const url = 'https://agendasmartproject.onrender.com/api/v1'

export default class TaskService {
  private response: IResponse = {} as IResponse
  constructor() {}

  async getPersonal(): Promise<IResponse> {
    try {
      const response = await fetch(url + '/activities/personal', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const jsonResponse = await response.json()
      auth.verifyToken(jsonResponse)
      return {
        data: jsonResponse.data,
        message: jsonResponse.message,
        error: jsonResponse.error
      }
    } catch (error) {
      return {
        data: null,
        message: 'Error de red',
        error: true
      }
    }
  }
  async getDeletedTasks(): Promise<any> {
    try {
      const reponse = await fetch(`${url}/activities/getTrashCan`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const json = await reponse.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.log(error)
    }
  }
  async getTask(id: any): Promise<any> {
    try {
      const response = await fetch(`${url}/activities/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const json = await response.json()
      auth.verifyToken(json)
      const { data } = json
      const { activity } = data
      return activity[0]
    } catch (error) {
      console.log(error)
    }
  }
  async getTasksByGroup(idGroup: any): Promise<any> {
    try {
      const response = await fetch(`${url}/activities/group/${idGroup}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const json = await response.json()
      return json
    } catch (error) {
      console.log(error)
    }
  }
  async createTask(task: ITask): Promise<any> {
    try {
      const response = await fetch(`${url}/activities`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify(task)
      })
      const json = await response.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.log(error)
    }
  }
  async updateTask(task: ITask, id: number): Promise<any> {
    try {
      const response = await fetch(`${url}/activities/${id}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify(task)
      })
      const json = await response.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.log(error)
    }
  }
  async deleteTask(id: number): Promise<any> {
    try {
      const response = await fetch(`${url}/activities/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        }
      })
      const json = await response.json()
      auth.verifyToken(json)
      return json
    } catch (error) {
      console.log(error)
    }
  }
  async destroyTask(ids: []): Promise<any> {
    try {
      const response = await fetch(`${url}/activities/trashCan`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('myToken')
        },
        body: JSON.stringify({
          ids
        })
      })
      const json = await response.json()
      auth.verifyToken(json)
      return response
    } catch (error) {
      console.error(error)
    }
  }
}
