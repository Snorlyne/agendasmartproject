import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import AuthService from '@/services/AuthServices'
import type { IResponse } from '@/interfaces/IResponse'
import { jwtDecode } from 'jwt-decode'

export const useUserStore = defineStore('user', () => {
  //state
  const user = ref() //Este valor debe asignarse desde el getUser by email pero solo si el usuario ya se logueo
  const token = ref('')
  const service = ref() //Este valor representa a la declaración más no a la inicialización del servicio.
  //actions
  async function login(email: string, password: string): Promise<IResponse> {
    try {
      service.value = new AuthService()
      const response = await service.value.login(email, password)
      token.value = response.data
      if (token.value) {
        localStorage.setItem('myToken', JSON.stringify(token.value).replace(/^"|"$/g, ''))
        return {
          data: token.value,
          message: 'Login correcto',
          error: false
        }
      } else {
        return {
          data: token.value,
          message: 'Credenciales incorrectas',
          error: true
        }
      }
    } catch (error) {
      return {
        data: token.value,
        message: 'Error de red',
        error: true
      }
    }
  }
  async function obtenerInfoUser(): Promise<any> {
    const decodedToken = jwtDecode(localStorage.getItem('myToken')!)
    user.value = decodedToken
    return user.value
  }

  async function logout() {
    token.value = ''
    localStorage.clear()
  }
  //getters
  const ValidToken = computed(() => !!token.value || !!localStorage.getItem('myToken'))

  return { login, user, logout, ValidToken, obtenerInfoUser, token }
})
