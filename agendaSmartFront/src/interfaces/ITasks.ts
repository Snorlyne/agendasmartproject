export default interface ITask {
  id?: number
  name?: string
  description?: string
  dueDate?: string
  status?: boolean
  creatorUserId?: number
  groupId?: number | null
}
export default interface ITaskFinish {
  status?: boolean
}
