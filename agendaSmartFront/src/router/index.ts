import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '@/views/DashboardView.vue'
import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import BinView from '@/views/BinView.vue'
import ActivityView from '@/views/ActivityView.vue'
import GroupView from '@/views/GroupView.vue'
import GroupTasksView from '@/views/GroupTasksView.vue'
import MemberGroupview from '@/views/MemberGroupview.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardView,
      meta: {
        Auth: true
      }
    },
    {
      path: '/actividad/:id',
      name: 'actividad',
      component: ActivityView,
      meta: {
        Auth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView,
      meta: {
        Auth: false
      }
    },
    {
      path: '/papelera',
      name: 'papelera',
      component: BinView,
      meta: {
        Auth: true
      }
    },
    {
      path: '/grupos/:id/miembros',
      name: 'Miembros',
      component: MemberGroupview,
      meta: {
        Auth: true
      }
    },
    {
      path: '/grupos/',
      name: 'grupos',
      component: GroupView,
      meta: {
        Auth: true
      }
    },
    {
      path: '/grupos/:id',
      name: 'grupo',
      component: GroupTasksView,
      meta: {
        Auth: true
      }
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notFound',
      component: NotFoundView
    }
  ]
})
router.beforeEach((to, _from, next) => {
  const auth = !!localStorage.getItem('myToken')
  const NeedAuth = to.meta.Auth || false

  if (NeedAuth && !auth) {
    next({ name: 'login' })
  } else if (auth && (to.name?.toString() == 'login' || to.name?.toString() == 'register')) {
    next({ name: 'dashboard' })
  } else {
    next()
  }
})
export default router
