import { Body, Controller, Get, HttpCode, HttpStatus, Post } from "@nestjs/common";
import { LoginDto } from "./dto/login.dto";
import { RegisterDto } from "./dto/register.dto";
import { AuthService } from "./auth.service";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { ActiveUser } from "src/common/decorators/active-user.decorator";
import { ActiveUserInterface } from "src/common/interfaces/active-user.interface";
import { Auth } from "src/common/decorators/auth.decorators";
@ApiTags('AuthService')
@Controller("auth")
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post("register")
    register(@Body() registerDto: RegisterDto) {
        return this.authService.register(registerDto);
    }

    @HttpCode(HttpStatus.OK)
    @Post("login")
    login(@Body() loginDto: LoginDto) {
        return this.authService.login(loginDto);
    }
    @ApiBearerAuth()
    @Get("validateUser")
    @Auth()
    validate(@ActiveUser() user: ActiveUserInterface) {
        return this.authService.validateUser(user);
    }
}