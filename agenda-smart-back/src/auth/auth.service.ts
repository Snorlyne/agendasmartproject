import { Injectable, UnauthorizedException } from "@nestjs/common";
import { RegisterDto } from "./dto/register.dto";
import { JwtService } from '@nestjs/jwt';
import * as bcryptjs from "bcryptjs";
import { UsersService } from "src/users/users.service";
import { LoginDto } from "./dto/login.dto";
import { IResponse } from "src/interfaces/IRepsponse";
import { ActiveUserInterface } from "src/common/interfaces/active-user.interface";

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService
    ) { }

    async register({ password, email, username }: RegisterDto): Promise<IResponse> {
        try {
            const userEmailOrUsername = await this.usersService.findOneByEmailOrUsername(email);

            if (userEmailOrUsername) {
                throw new Error("Este usuario ya esta registrado");
            }

            const hashedPassword = await bcryptjs.hash(password, 10);

            await this.usersService.create({
                email,
                username,
                password: hashedPassword,
            });

            return {
                data: null,
                message: "Usuario registrado correctamente",
                error: false,
            };
        } catch (error) {
            return {
                data: null,
                message: error.message,
                error: true,
            };
        }
    }

    async login({ emailOrUsername, password }: LoginDto): Promise<IResponse> {
        try {
            const user = await this.usersService.findOneByEmailOrUsername(emailOrUsername);

            if (!user) {
                throw new Error("Usuario o Email invalido");
            }

            const isPasswordValid = await bcryptjs.compare(password, user.password);

            if (!isPasswordValid) {
                throw new Error("Contraseña invalida");
            }
            const payload = { id: user.id, email: user.email, username: user.username };
            const token = await this.jwtService.signAsync(payload);

            return {
                data: token,
                message: "Login correcto",
                error: false,
            };
        } catch (error) {
            return {
                data: null,
                message: error.message,
                error: true,
            };
        }
    }
    async validateUser(user: ActiveUserInterface): Promise<IResponse> {
        const userInDB = await this.usersService.findOneByEmailOrUsername(user.email);
        if (!userInDB) {
            throw new UnauthorizedException("Sesión expirada");
        }
        return {
            data: user,
            message: "Usuario autenticado correctamente",
            error: false,
        };
    }
}