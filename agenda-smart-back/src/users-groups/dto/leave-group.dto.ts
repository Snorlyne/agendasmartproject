import { IsNumber, IsString } from "class-validator";

export class LeaveGroupDto {
    @IsNumber()
    idGroup: number;
}
