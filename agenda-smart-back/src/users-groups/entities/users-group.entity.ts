import { Group } from "src/groups/entities/group.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UsersGroup {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, user => user.usersGroup, {
        eager: true,
        onDelete: "NO ACTION",
        onUpdate: "NO ACTION"
    })
    users: User;

    @ManyToOne(() => Group, group => group.usersGroup, {
        eager: true
    })
    groups: Group;

    @Column({ default: false })
    isLeader: boolean;
}
