import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UsersGroupsService } from './users-groups.service';
import { JoinGroupDto } from './dto/join-group.dto';
import { ActiveUserInterface } from '../common/interfaces/active-user.interface';
import { ActiveUser } from '../common/decorators/active-user.decorator';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Auth } from '../common/decorators/auth.decorators';
import { LeaveGroupDto } from './dto/leave-group.dto';
@ApiTags('UsersGroupsService')
@Auth()
@ApiBearerAuth()
@Controller('users-groups')
export class UsersGroupsController {
  constructor(private readonly usersGroupsService: UsersGroupsService) {}

  @Post('join')
  join(@Body() joinGroupDto: JoinGroupDto, @ActiveUser() user: ActiveUserInterface ){
    return this.usersGroupsService.join(joinGroupDto.keyGroup, user.id);
  }
  @Post('leave')
  leave(@Body() idGroup: LeaveGroupDto, @ActiveUser() user: ActiveUserInterface ){
    return this.usersGroupsService.leave(idGroup.idGroup, user.id);
  }
  
  @Get(':id')
  findAllByUser(@Param('id') id: number){
    return this.usersGroupsService.findAllMembers(id);
  }
  @Delete('expel/:id')
  expel(@Param('id') id: number){
    return this.usersGroupsService.expel(id);
  }

}
