import { Injectable } from '@nestjs/common';
import { JoinGroupDto } from './dto/join-group.dto';
import { IResponse } from 'src/interfaces/IRepsponse';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from 'src/groups/entities/group.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { UsersGroup } from './entities/users-group.entity';

@Injectable()
export class UsersGroupsService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
    @InjectRepository(UsersGroup)
    private readonly usersGroupsRepository: Repository<UsersGroup>
  ) { }
  async create(idGroup: number, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const group = await this.groupsRepository.findOneBy({ id: idGroup });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const newGroup = this.usersGroupsRepository.create({
        users: user,
        groups: group,
        isLeader: true
      });
      await this.usersGroupsRepository.save(newGroup);
      return {
        data: newGroup,
        message: "Grupo creado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
  async join(keyGroup: string, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const group = await this.groupsRepository.findOneBy({ key: keyGroup });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const userInGroup = await this.usersGroupsRepository.findOneBy({ groups: group, users: user })
      if (userInGroup) {
        throw new Error("Ya perteneces al grupo");
      }
      const newGroup = this.usersGroupsRepository.create({
        users: user,
        groups: group,
        isLeader: false
      });
      await this.usersGroupsRepository.save(newGroup);
      return {
        data: null,
        message: "Te has unido satisfactoriamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async leave(idGroup: number, idUser: number): Promise<IResponse> {
    try {
      const userInGroup = await this.usersGroupsRepository.findOneBy({
        users: {
          id: idUser
        },
        groups: {
          id: idGroup
        }
      });
      if (!userInGroup) {
        throw new Error("Usuario no pertenece al grupo");
      }
      if (userInGroup.isLeader) {
        throw new Error("No puedes salirte como propietario del grupo");
      }
      await this.usersGroupsRepository.remove(userInGroup);
      return {
        data: null,
        message: "Te has salido satisfactoriamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
  async expel(idUserInGroup: number): Promise<IResponse> {
    try {
      const userInGroup = await this.usersGroupsRepository.findOneBy({
        users: {
          id: idUserInGroup
        }
      });
      await this.usersGroupsRepository.delete(userInGroup);
      return {
        data: null,
        message: "Usuario expulsado correctamente",
        error: false
      }
    } catch (error) {
      return { 
        data: null,
        error: true,
        message: error.message
      }
    }
  }

  async findAllByUser(idUser: number): Promise<IResponse> {
    try {
      const userInGroup = await this.usersGroupsRepository.query(`
      SELECT
      groupsId
      FROM users_group
      WHERE usersId = ${idUser};`);
      if (userInGroup.length == 0) {
        throw new Error("No perteneces a ningun grupo");
      }
      return {
        data: userInGroup,
        message: "Grupos encontrados correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async findAllMembers(idGroup: number): Promise<IResponse> {
    try {
      // Obtener todos los usuarios asociados al grupo
      const usersInGroup = await this.usersGroupsRepository.find({
        where: {
          groups: {
            id: idGroup
          }
        },
        relations: ['users'] // Incluir la relación con la entidad de usuarios
      });
      // Obtener los nombres y los IDs de los usuarios
      const memberData = usersInGroup.map(userGroup => ({
        id: userGroup.users.id,
        name: userGroup.users.username
      }));

      return {
        data: memberData,
        message: "Usuarios encontrados correctamente",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }

  async findOneByUser(idGroup: number, idUser: number): Promise<IResponse> {
    try {
      const userInGroup = await this.usersGroupsRepository.find({
        where: {
          users: { id: idUser },
          groups: { id: idGroup }
        }
      });
      if (userInGroup.length == 0) {
        throw new Error("No perteneces a este grupo");
      };
      return {
        data: userInGroup,
        message: "Grupo encontrado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
  async remove(idUser: number, idGroup: number): Promise<IResponse> {
    try {
      const userInGroup = await this.usersGroupsRepository.find({
        where: {
          users: { id: idUser },
        }
      });
      if (userInGroup.length == 0) {
        throw new Error("No perteneces a este grupo");
      };
      if (userInGroup[0].isLeader) {
        const members = await this.usersGroupsRepository.find({
          where: {
            groups: {
              id: idGroup
            }
          }
        });
        await this.usersGroupsRepository.remove(members);
      } else {
        throw new Error("No eres propietario de este grupo");
      }
      return {
        data: null,
        message: "Grupo eliminado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
  async findAllLeader(idUser: number): Promise<IResponse> {
    try {
      const usersInGroup = await this.usersGroupsRepository.find({
        where: {
          isLeader: true,
          users: {
            id: idUser
          }
        },
        relations: ['users']
      });
      return {
        data: usersInGroup,
        message: "Grupos encontrados correctamente",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }
}
