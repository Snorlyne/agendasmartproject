export interface IResponse {
    data: {};
    message: string;
    error: boolean;
}