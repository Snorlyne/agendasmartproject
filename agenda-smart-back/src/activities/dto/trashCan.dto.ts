import { IsArray, isNumber } from "class-validator";

export class TrashCanDto {
    @IsArray()
    ids: number[]
}