import { IsNotEmpty, IsString, IsDate, IsInt, IsBoolean, IsOptional, IsDateString } from 'class-validator';

export class CreateActivityDto {
    @IsNotEmpty()
    @IsString()
    description: string;

    @IsNotEmpty()
    @IsDateString()
    dueDate: Date;
    
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsOptional()
    @IsBoolean()
    status: boolean;

    @IsOptional()
    @IsInt()
    groupId: number;
    
    @IsOptional()
    @IsInt()
    creatorUserId: number;
}

