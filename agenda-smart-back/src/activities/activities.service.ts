import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateActivityDto } from './dto/create-activity.dto';
import { UpdateActivityDto } from './dto/update-activity.dto';
import { IResponse } from 'src/interfaces/IRepsponse';
import { InjectRepository } from '@nestjs/typeorm';
import { Activity } from './entities/activity.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Group } from 'src/groups/entities/group.entity';
import { UsersGroup } from 'src/users-groups/entities/users-group.entity';
import { TrashCanDto } from './dto/trashCan.dto';

@Injectable()
export class ActivitiesService {
  constructor(
    @InjectRepository(Activity)
    private readonly activtiesRepository: Repository<Activity>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
    @InjectRepository(UsersGroup)
    private readonly usersGroupsRepository: Repository<UsersGroup>
  ) { }
  async create(createActivityDto: CreateActivityDto, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      const userMenmber = await this.usersRepository.findOneBy({ id: createActivityDto.creatorUserId });
      const group = await this.groupsRepository.findOneBy({ id: createActivityDto.groupId });

      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      if (group) {
        const userInGroup = await this.usersGroupsRepository.query(`
        SELECT
        id,
        usersId,
        groupsId,
        isLeader
        FROM users_group
        WHERE usersId = ${idUser} AND groupsId = ${group.id};`);
        if (userInGroup.length == 0) {
          throw new Error("No eres mienmbro de este grupo");
        } else {
          if (userMenmber == null) {
            const newActivity = this.activtiesRepository.create({
              ...createActivityDto,
              creatorUser: user,
              group: group
            });
            await this.activtiesRepository.save(newActivity);
          } else {
            const newActivity = this.activtiesRepository.create({
              ...createActivityDto,
              creatorUser: userMenmber,
              group: group
            });
            await this.activtiesRepository.save(newActivity);
          }
        }
      } else {
        const newActivity = this.activtiesRepository.create({
          ...createActivityDto,
          creatorUser: user
        });
        await this.activtiesRepository.save(newActivity);
      }
      return {
        data: createActivityDto,
        message: 'Actividad creada correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }

  async findAllPersonal(id: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: id });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const activities = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId
      FROM activity
      WHERE creatorUserId = ${id}
      AND deletedAt IS NULL
    `);
      if (activities.length == 0) {
        throw new Error("Actividad no encontrada");
      }
      return {
        data: activities,
        message: 'Actividades encontradas correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async findAllGroup(idGroup: number, idUser: number): Promise<IResponse> {
    try {
      const group = await this.groupsRepository.findOneBy({ id: idGroup });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const activities = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId
      FROM activity
      WHERE groupId = ${group.id}
      AND deletedAt IS NULL
    `);
      if (activities.length == 0) {
        throw new Error("No hay actividades por mostrar");
      } else {
        const userInGroup = await this.usersGroupsRepository.query(`
          SELECT
          id,
          usersId,
          groupsId,
          isLeader
          FROM users_group
          WHERE usersId = ${idUser} AND groupsId = ${group.id};`);
        if (userInGroup.length == 0) {
          throw new Error("No eres mienmbro de este grupo");
        }
      }
      return {
        data: activities,
        message: 'Actividades encontradas correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async findOneById(id: number, idUser: number): Promise<IResponse> {
    try {
      const activity = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId
      FROM activity
      WHERE id = ${id}
      AND deletedAt IS NULL
    `);
      if (activity.length == 0) {
        throw new Error("Actividad no encontrada");
      }
      if (activity[0].creatorUserId === idUser) {
        return {
          data: { activity, isOwnership: true },
          message: 'Actividad encontrada correctamente',
          error: false
        };
      } else if (activity[0].groupId !== null) {
        const userInGroup = await this.usersGroupsRepository.query(`
        SELECT
        id,
        usersId,
        groupsId,
        isLeader
        FROM users_group
        WHERE usersId = ${idUser} AND groupsId = ${activity[0].groupId};
        `);
        if (userInGroup.length == 0) {
          throw new Error("No eres mienmbro de este grupo");
        }
        return {
          data: { activity, isOwnership: false },
          message: 'Actividad encontrada correctamente',
          error: false
        };
      } else {
        throw new Error("Actividad no encontrada");
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async findTrasCan(idUser: number): Promise<IResponse> {
    try {
      const activities = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId,
      deletedAt
      FROM activity
      WHERE creatorUserId = ${idUser}
      AND deletedAt IS NOT NULL
    `);
      if (activities.length == 0) {
        throw new Error("No hay actividades por mostrar");
      }
      return {
        data: activities,
        message: 'Actividades encontradas correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async update(id: number, updateActivityDto: UpdateActivityDto, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const activity = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId
      FROM activity
      WHERE id = ${id}
      AND deletedAt IS NULL
    `);
      if (activity.length == 0) {
        throw new Error("Actividad no encontrada");
      } else {
        if (activity[0].groupId !== null) {
          const group = await this.groupsRepository.findOneBy({ id: activity[0].groupId });
          const userInGroup = await this.usersGroupsRepository.query(`
          SELECT
          id,
          usersId,
          groupsId,
          isLeader
          FROM users_group
          WHERE usersId = ${idUser} AND groupsId = ${group.id};`);
          if (userInGroup.length == 0) {
            throw new Error("No eres mienmbro de este grupo");
          }
          if (userInGroup[0].isLeader == true || activity[0].creatorUserId == idUser) {
            await this.activtiesRepository.update(activity[0].id, {
              name: updateActivityDto.name,
              description: updateActivityDto.description,
              dueDate: updateActivityDto.dueDate,
              status: updateActivityDto.status
            })
          } else {
            throw new Error("No tienes permisos para actualizar esta actividad");
          }
        } else {
          if (activity[0].creatorUserId === idUser) {
            await this.activtiesRepository.update(activity[0].id, {
              name: updateActivityDto.name,
              description: updateActivityDto.description,
              dueDate: updateActivityDto.dueDate,
              status: updateActivityDto.status,
            })
          } else {
            throw new Error("No tienes permisos para actualizar esta actividad");
          }
        }
      }
      return {
        data: updateActivityDto,
        message: 'Actividad actualizada correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async remove(id: number, idUser: number): Promise<IResponse> {
    try {
      const activity = await this.activtiesRepository.query(`
      SELECT
      id,
      name,
      description,
      dueDate,
      status,
      creatorUserId,
      groupId
      FROM activity
      WHERE id = ${id}
      AND deletedAt IS NULL
      `);
      if (activity.length == 0) {
        throw new Error("Actividad no encontrada");
      } else {
        if (activity[0].groupId !== null) {
          const group = await this.groupsRepository.findOneBy({ id: activity[0].groupId });
          const userInGroup = await this.usersGroupsRepository.query(`
          SELECT
          id,
          usersId,
          groupsId,
          isLeader
          FROM users_group
          WHERE usersId = ${idUser} AND groupsId = ${group.id};
          `);
          if (userInGroup.length == 0) {
            throw new Error("No eres mienmbro de este grupo");
          }
          if (userInGroup[0].isLeader == true || activity[0].creatorUserId == idUser) {
            await this.activtiesRepository.softDelete(activity[0].id)
          } else {
            throw new Error("No tienes permisos para eliminar esta actividad");
          }
        } else {
          if (activity[0].creatorUserId === idUser) {
            await this.activtiesRepository.softDelete(activity[0].id)
          } else {
            throw new Error("No tienes permisos para eliminar esta actividad");
          }
        }
      }
      return {
        data: activity[0].name,
        message: 'Actividad eliminada correctamente',
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
  async removeTrashCan(trashCanDto: TrashCanDto, idUser: number): Promise<IResponse> {
    try {
      if (trashCanDto.ids.length == 0) {
        throw new Error("No se han seleccionado actividades");
      }
      const promises = trashCanDto.ids.map(async idActivity => {
        const activity = await this.activtiesRepository.query(`
        SELECT
        id,
        name,
        description,
        dueDate,
        status,
        creatorUserId,
        groupId
        FROM activity
        WHERE id = ${idActivity};
        `);
        if (activity.length == 0) {
          throw new Error(`Actividad ${idActivity} no encontrada`);
        } else {
          if (activity[0].groupId !== null) {
            const group = await this.groupsRepository.findOneBy({ id: activity[0].groupId });
            const userInGroup = await this.usersGroupsRepository.query(`
            SELECT
            id,
            usersId,
            groupsId,
            isLeader
            FROM users_group
            WHERE usersId = ${idUser} AND groupsId = ${group.id};
            `);
            if (userInGroup.length == 0) {
              throw new Error("No eres mienmbro de este grupo");
            }
            if (userInGroup[0].isLeader == true || activity[0].creatorUserId == idUser) {
              await this.activtiesRepository.remove(activity[0])
            } else {
              throw new Error("No tienes permisos para eliminar esta actividad");
            }
          } else {
            if (activity[0].creatorUserId === idUser) {
              await this.activtiesRepository.remove(activity[0])
            } else {
              throw new Error("No tienes permisos para eliminar esta actividad");
            }
          }
        }
      });
      await Promise.all(promises);
      const message = trashCanDto.ids.length > 1 ? 'Actividades eliminadas correctamente' : 'Actividad eliminada correctamente';
      return {
        data: null,
        message: message,
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
}
