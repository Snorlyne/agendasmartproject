// task.entity.ts
import { Group } from 'src/groups/entities/group.entity';
import { User } from 'src/users/entities/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, DeleteDateColumn } from 'typeorm';

@Entity()
export class Activity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 50, nullable: false })
    name: string;

    @Column()
    description: string;

    @Column({ type: 'date' })
    dueDate: Date;

    @Column({ default: false })
    status: boolean;

    @ManyToOne(() => User, user => user.activity, {nullable: true})
    creatorUser: User;

    @ManyToOne(() => Group, group => group.activity, { nullable: true })
    group: Group;

    @DeleteDateColumn()
    deletedAt: Date;
}

