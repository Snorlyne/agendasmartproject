import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ActivitiesService } from './activities.service';
import { CreateActivityDto } from './dto/create-activity.dto';
import { UpdateActivityDto } from './dto/update-activity.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ActiveUserInterface } from '../common/interfaces/active-user.interface';
import { ActiveUser } from '../common/decorators/active-user.decorator';
import { Auth } from '../common/decorators/auth.decorators';
import { TrashCanDto } from './dto/trashCan.dto';
@Auth()
@ApiBearerAuth()
@ApiTags('ActivitiesService')
@Controller('activities')
export class ActivitiesController {
  constructor(private readonly activitiesService: ActivitiesService) { }

  @Post()
  async create(@Body() createActivityDto: CreateActivityDto, @ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.create(createActivityDto, user.id);
  }
  @Post('trashCan')
  removeTrashCan(@Body() ids: TrashCanDto, @ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.removeTrashCan(ids, user.id);
  }

  @Get('getTrashCan')
  findTrasCan(@ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.findTrasCan(user.id);
  }

  @Get('personal')
  findAllPersonal(@ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.findAllPersonal(user.id);
  }
  @Get('group/:id')
  findAllGroup(@Param('id') id: number, @ActiveUser() user: ActiveUserInterface ) {
    return this.activitiesService.findAllGroup(id, user.id);
  }
  @Get(':id')
  findOneById(@Param('id') id: number, @ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.findOneById(id, user.id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateActivityDto: UpdateActivityDto, @ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.update(id, updateActivityDto, user.id);
  }

  @Delete(':id')
  remove(@Param('id') id: number, @ActiveUser() user: ActiveUserInterface) {
    return this.activitiesService.remove(id, user.id);
  }
}
