import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { User } from "./entities/user.entity";
import { IResponse } from "../interfaces/IRepsponse";
import { UsersGroupsService } from "../users-groups/users-groups.service";
import { GroupsService } from "../groups/groups.service";
import { Group } from "../groups/entities/group.entity";
import { ActivitiesService } from "../activities/activities.service";
import { TrashCanDto } from "../activities/dto/trashCan.dto";

@Injectable()
export class UsersService {
  constructor(
    private readonly usersGroupService: UsersGroupsService,
    private readonly groupsService: GroupsService,
    private readonly activitiesService: ActivitiesService,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>
  ) { }

  async create(createUserDto: CreateUserDto) {
    return await this.usersRepository.save(createUserDto);
  }
  async findOneByEmailOrUsername(emailOrUsername: string): Promise<User | null> {
    let user = await this.usersRepository.findOne({
      where: { email: emailOrUsername },
      select: ['id', 'username', 'email', 'password'],
    });
    if (!user) {
      user = await this.usersRepository.findOne({
        where: { username: emailOrUsername },
        select: ['id', 'username', 'email', 'password'],
      });
    }
    return user || null; // Devuelve el usuario encontrado o null si no se encuentra ninguno
  }
  async findOneByID(id: number, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: id });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const isMe = id === idUser;
      return {
        data: { ...user, isMe: isMe },
        message: "Usuario encontrado",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }
  async update(id: number, updateUserDto: UpdateUserDto, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const isMe = id === idUser;
      if (!isMe) {
        throw new Error("No tienes permisos para actualizar este usuario");
      }
      return {
        data: await this.usersRepository.update(id, updateUserDto),
        message: "Usuario actualizado correctamente",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }
  async remove(id: number, idUser: number): Promise<IResponse> {
    try {
      const user = await this.usersRepository.findOneBy({ id: idUser });
      if (!user) {
        throw new Error("Usuario no encontrado");
      }
      const isMe = id === idUser;
      if (!isMe) {
        throw new Error("No tienes permisos para eliminar este usuario");
      }
      const groupsLeader = await this.usersGroupService.findAllLeader(idUser);
      if (groupsLeader.error) {
        throw new Error(groupsLeader.message);
      }
      const activitiesData = await this.activitiesService.findAllPersonal(idUser);
      if (!activitiesData.error) {
        const ids: number[] = Array.isArray(activitiesData.data) ? activitiesData.data.map((activity: any) => activity.id) : [];
        const trashCanDto = new TrashCanDto();
        trashCanDto.ids = ids;
        await this.activitiesService.removeTrashCan(trashCanDto, idUser)
      }
      const ids: number[] = Array.isArray(activitiesData.data) ? activitiesData.data.map((activity: any) => activity.id) : [];
      const trashCanDto = new TrashCanDto();
      trashCanDto.ids = ids;
      await this.activitiesService.removeTrashCan(trashCanDto, idUser)

      if (Array.isArray(groupsLeader.data)) {
        for (const item of groupsLeader.data) {
          await this.groupsService.remove(item.id, idUser)
            .catch(error => {
              throw new Error(error.message);
            });
        }
      }
      return {
        data: await this.usersRepository.delete(id),
        message: "Usuario eliminado correctamente",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }
}