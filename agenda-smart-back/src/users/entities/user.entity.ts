import { Activity } from "src/activities/entities/activity.entity";
import { UsersGroup } from "src/users-groups/entities/users-group.entity";
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 50, nullable: true })
    name: string;

    @Column({ unique: true, length: 25 })
    username: string;

    @Column({ unique: true, nullable: false,  })
    email: string;

    @Column({ nullable: false, select: false })
    password: string;

    @Column({ default: "user" })
    rol: string;

    @OneToMany(() => Activity, activity => activity.creatorUser,{
        onDelete: 'CASCADE'
    })
    activity: Activity[];

    @OneToMany(() => UsersGroup, usersGroup => usersGroup.users, {
        onDelete: 'CASCADE'	
    })
    usersGroup: User[];

    @DeleteDateColumn()
    deletedAt: Date;

}