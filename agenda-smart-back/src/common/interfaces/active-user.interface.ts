export interface ActiveUserInterface {
    id: number;
    email: string;
    username: string;
}