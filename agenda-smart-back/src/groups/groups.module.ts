import { Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { UsersGroupsService } from '../users-groups/users-groups.service';
import { UsersGroupsModule } from '../users-groups/users-groups.module';
import { AuthModule } from 'src/auth/auth.module';
import { ActivitiesModule } from 'src/activities/activities.module';

@Module({
  imports: [TypeOrmModule.forFeature([Group]), UsersGroupsModule, AuthModule, ActivitiesModule],
  controllers: [GroupsController],
  providers: [GroupsService],
  exports: [GroupsService]
})
export class GroupsModule {}
