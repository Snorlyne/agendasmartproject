import { UsersGroup } from "src/users-groups/entities/users-group.entity";
import { Activity } from "../../activities/entities/activity.entity";
import { BeforeInsert, Column, DeleteDateColumn, Entity, ManyToMany, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Group {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 25, nullable: false })
    name: string;

    @Column({ length: 25, nullable: true })
    description: string;

    @OneToMany(() => Activity, activity => activity.group)
    activity: Activity[];

    @OneToMany(() => UsersGroup, usersGroup => usersGroup.groups)
    usersGroup: Group[];

    @Column()
    key: string;

    @DeleteDateColumn()
    deletedAt: Date;

    @BeforeInsert()
    generateToken() {
        const timestamp = Date.now().toString();
        this.key = `${timestamp}_${this.name}`;
    }
}