import { Injectable } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { IResponse } from '../interfaces/IRepsponse';
import { In, Repository } from 'typeorm';
import { Group } from './entities/group.entity';
import { User } from '../users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersGroup } from '../users-groups/entities/users-group.entity';
import { UsersGroupsService } from '../users-groups/users-groups.service';
import { ActivitiesService } from '../activities/activities.service';
import { TrashCanDto } from '../activities/dto/trashCan.dto';

@Injectable()
export class GroupsService {
  constructor(
    private readonly usersGroupService: UsersGroupsService,
    private readonly activitiesService: ActivitiesService,
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>
  ) { }
  async create(createGroupDto: CreateGroupDto, idUser: number): Promise<IResponse> {
    try {
      const newGroup = this.groupsRepository.create(createGroupDto);
      const groupSaved = await this.groupsRepository.save(newGroup);
      const usersGroup = await this.usersGroupService.create(groupSaved.id, idUser);
      if (usersGroup.error) {
        await this.groupsRepository.delete(groupSaved.id);
        throw new Error(usersGroup.message);
      }
      return {
        data: newGroup,
        message: "Grupo creado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async findAll(idUser: number): Promise<IResponse> {
    try {
      const usersInGroupData = await this.usersGroupService.findAllByUser(idUser);
      if (usersInGroupData.error) {
        throw new Error(usersInGroupData.message);
      }

      let groupIds: number[] = [];
      if (Array.isArray(usersInGroupData.data)) {
        for (const item of usersInGroupData.data) {
          groupIds.push(item.groupsId);
        }
      } else {
        throw new Error("El atributo 'data' no es un array");
      }
      const groups = await this.groupsRepository.find({
        where: {
          id: In(groupIds)
        }
      });
      return {
        data: groups,
        message: "Grupos encontrados correctamente",
        error: false
      };
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      };
    }
  }

  async findOne(id: number, idUser: number): Promise<IResponse> {
    try {
      const group = await this.groupsRepository.findOneBy({ id: id });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const usersInGroupData = await this.usersGroupService.findOneByUser(id, idUser);
      if (usersInGroupData.error) {
        throw new Error(usersInGroupData.message);
      }
      if (usersInGroupData.data[0].isLeader) {
        return {
          data: {
            ...group,
            isOwner: true,
          },
          message: "Grupo encontrado correctamente",
          error: false
        }
      } else {
        return {
          data: {
            ...group,
            isOwner: false,
          },
          message: "Grupo encontrado correctamente",
          error: false
        }
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async update(id: number, updateGroupDto: UpdateGroupDto, idUser: number): Promise<IResponse> {
    try {
      const group = await this.groupsRepository.findOneBy({ id: id });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const userInGroupData = await this.usersGroupService.findOneByUser(id, idUser);
      if (userInGroupData.error) {
        throw new Error(userInGroupData.message);
      }
      if (!userInGroupData.data[0].isLeader) {
        throw new Error("No tienes permisos para actualizar este grupo");
      }
      await this.groupsRepository.update(id, updateGroupDto);
      return {
        data: updateGroupDto,
        message: "Grupo actualizado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }

  async remove(id: number, idUser: number): Promise<IResponse> {
    try {
      const group = await this.groupsRepository.findOneBy({ id: id });
      if (!group) {
        throw new Error("Grupo no encontrado");
      }
      const userInGroupData = await this.usersGroupService.findOneByUser(id, idUser);
      if (userInGroupData.error) {
        throw new Error(userInGroupData.message);
      }
      if (!userInGroupData.data[0].isLeader) {
        throw new Error("No tienes permisos para eliminar este grupo");
      }
      const activitiesData = await this.activitiesService.findAllGroup(id,idUser);
      if (!activitiesData.error) {
        const ids: number[] = Array.isArray(activitiesData.data) ? activitiesData.data.map((activity: any) => activity.id) : [];
        const trashCanDto = new TrashCanDto();
        trashCanDto.ids = ids;
        await this.activitiesService.removeTrashCan(trashCanDto, idUser)
      }
      const deletedUsersInGroupData = await this.usersGroupService.remove(idUser, id)
      if (deletedUsersInGroupData.error) {
        throw new Error(deletedUsersInGroupData.message);
      }
      await this.groupsRepository.delete(id);
      return {
        data: null,
        message: "Grupo eliminado correctamente",
        error: false
      }
    } catch (error) {
      return {
        data: null,
        message: error.message,
        error: true
      }
    }
  }
}
