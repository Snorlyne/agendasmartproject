import { IsDate, IsNotEmpty, IsOptional, IsString, Length, isString } from "class-validator";

export class CreateGroupDto {
    @IsNotEmpty()
    @IsString()
    @Length(1, 25)
    name: string;

    @IsOptional()
    @IsString()
    @Length(1, 65)
    description: string;
    

}
